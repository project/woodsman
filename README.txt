
*Installation Instructions*

To install the theme perfom the following steps:

1. Download omega
2. Install Omega
3. Install Woodsman
4. Enable and set default Woodsman


*Developer Instructions*

To initialize the theme from it's raw download state to a state that can be
used to develop and improve the theme, perform the following steps:

1. Perform the installation steps above
2. Download modules devel
3. Enable devel
4. Generate content 50 nodes with comments
5. Write a summry for the about page make it promoted / sticky to the top
6. Upload a logo
7. Generate 4 menus
8. Add Main Menu to branding region
9. Hide the image field from teaser display mode
10. Add blocks to sidebar
